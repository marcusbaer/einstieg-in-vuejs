Einstieg in VueJS
=================

Der Einstieg in VueJS besteht aus zwei Veranstaltungen:

**"Prototyping mit Nuxt und Docker"**

Das Seminar demonstriert, wie ein leichter Einstieg in VueJS mit Hilfe von Nuxt gelingen kann.
Anhand ausgewählter Optionen wird zudem vorgestellt, wie erstellte Prototypen für andere zugänglich gemacht werden können.

**"Mit VueJS arbeiten"**

Der Workshop lädt zum Mitmachen ein und zeigt, wie VueJS ins eigene Projekt integriert werden kann.
Ein Überblick über die wichtigsten Features von VueJS hilft, erste Schritte im System problemlos bewältigen zu können.
Zudem werden ein paar grundlegende Module vorgestellt, die häufig im Alltag benötigt werden.

Einordnung
=================

VueJS ist ..

Mehr lernen
=================

- über AVA: https://github.com/avajs/ava

Vorbereitung der Themen:
=================

Folgendes sollte ich durcharbeiten und ggf. einbauen:

- https://blog.sicara.com/a-progressive-web-application-with-vue-js-webpack-material-design-part-1-c243e2e6e402
- https://medium.com/bam-tech/a-progressive-web-application-with-vue-js-webpack-material-design-part-2-a5f19e70e08b
- https://blog.sicara.com/a-progressive-web-application-with-vue-js-webpack-material-design-part-3-service-workers-offline-ed3184264fd1
