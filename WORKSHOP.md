# Mit VueJS arbeiten

    npm install -g ngrok
    ngrok http 8080

- Nuxt ist gute Basis für VueJS-Projekte: bevorzugen (Server-Rendering, ssr.vuejs.org)
- Integration in bestehende Projekte mit Nuxt schwierig

## VueJS get started

Hello World mit CDN

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <div id="app">
      <p v-on:click="reverseMessage">{{ message }}</p>
      <input v-model="message">
      <hr>
      <ol>
        <todo-item v-for="item in todos" v-bind:todo="item" v-bind:key="item.id"></todo-item>
      </ol>
    </div>
    <script>
    
    // keine Web Component
    Vue.component('todo-item', {
      props: ['todo'],
      template: '<li>{{ todo.text }}</li>'
    })
    
    var app = new Vue({
      el: '#app',
      data: {
        message: 'Hello Vue!',
        todos: [
          { text: 'Learn JavaScript' },
          { text: 'Learn Vue' },
          { text: 'Build something awesome' }
        ]
      },
      methods: {
        reverseMessage: function () {
          this.message = this.message.split('').reverse().join('')
        }
      }
    })
    </script>
    
Geht natürlich auch mit Fiddle und Co: https://jsfiddle.net/Akryum/ft0dmoex/    

## vue-cli

    npm install -g vue-cli

## VueJS mit ParcelJS

## Lokale Entwicklungsumgebung III

Entwicklungsumgebung für einen leichten Projekteinstieg virtualisieren.

- exemplarisch am DocMorris-Konzept aufzeigen
- docker-compose.yml beschreibt unsere lokale Entwicklungsumgebung
- Dockerfile für das System
- Compose-Task für den Dev-Server
- Compose-Task für Builds?
- Volume-Binding in das Projekt?
  
## Grundlagen von VueJS
  
- Lifecycle
- Template Syntax
- Computed Properties and Watchers
- Class and Style Bindings
- Conditional Rendering
- Form Input Bindings
- Components: Props
- Custom Events
- Transitions & Animation
- Unit Testing
- Routing: z.B. https://blog.sicara.com/a-progressive-web-application-with-vue-js-webpack-material-design-part-1-c243e2e6e402

## Vuex

## Custom Elements (Web Components)

## Andere Projekte

    npm install vuefire --save
    workbox
    sw-precache
    sw-toolbox

## Fragen

    Marcus Baer <marcus.baer@publicispixelpark.de>
