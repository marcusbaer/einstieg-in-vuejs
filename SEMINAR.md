# Prototyping mit Nuxt und Docker

## Projektbasis

    mkdir my-prototype && cd my-prototype
    npm init
    touch README.md

    npm i nuxt @nuxtjs/pwa --save
    echo "<template><h1>Hallo</h1></template>" > pages/index.vue

    nano package.json
    # add tasks:
    "dev": "nuxt",
    "build": "nuxt build",
    "start": "nuxt start",
    "generate": "nuxt generate"

    npm run dev

## Systemsetup

Laufumgebung im Dockerfile definieren:

    FROM node:alpine
    
    # System variables
    #ENV HOST 0.0.0.0
    #ENV PORT 3000
    ENV NODE_ENV dev
    ENV POSTS_SERVICE https://jsonplaceholder.typicode.com/posts
    
    #RUN apk update && \
    #    apk upgrade && \
    #    apk add git
    
    # Install dependencies by using layer caching (no changes)
    ADD . /usr/src/app
    
    RUN cd /usr/src/app && \
        npm install && \
        npm run build
    
    WORKDIR /usr/src/app
    
    EXPOSE 3000
    
    VOLUME [ "/usr/src/app" ]
    
    CMD ["npm", "start"]

Ausschließen unerwünschter Resourcen in `.dockerignore`

    .env
    .idea
    .nuxt
    node_modules

Testen der Systems

    docker build -t my/prototype .
    docker run -d -p 8080:3000 -e PORT=3000 --restart always --name my-prototype my/prototype
    docker stop my-prototype
    docker rm my-prototype

## Deployment mit Now.sh  

    npm i -g now
    
    # neuer Node task:
    "deploy": "now rm my-prototype --yes && now --docker -p -C -e NODE_ENV='now' && now alias",

    # Now Konfiguration in package.json
    "now": {
        "alias": "my-prototype",
        "env": {
          "NODE_ENV": "production"
        },
        "type": "docker"
    },

    now login
    now switch
    npm run deploy

## Code linting

    npm i eslint --save-dev
    
    # Node tasks:
    "lint": "eslint --ext .js,.vue --ignore-path .gitignore .",
    "precommit": "npm run lint"

## Entwicklung mit Nuxt

- Grundstruktur: pages, static, components, layouts nuxt.config.js
- Komponente Posts mit Axios erstellen und einbinden mit v-for ohne Benutzerinteraktion
- Scoped CSS mit SASS und globalen Variablendefinition
- Umstellen der Posts auf store

## Lokale Entwicklungumgebung I

Für lokale Tests die fremde API durch eine eigene ersetzen:

Zunächst `nuxt.config.js` um serverMiddleware erweitern:
  
    serverMiddleware: [{ path: '/api', handler: '~/api/index.js' }]
    
Anlegen der Datei `api/index.js` nicht vergessen:

    const axios = require('axios')
    const POSTS_JSON = require('../static/posts.json')
    
    function getPosts () {
      return new Promise((resolve, reject) => {
        resolve(POSTS_JSON)
      })
    }
    
    module.exports = function (req, res, next) {
    
      // req is the Node.js http request object
      // res is the Node.js http response object
      console.log(req.url, req.method)
    
      // next is a function to call to invoke the next middleware
      // Don't forget to call next at the end if your middleware is not an endpoint!
      // next()
    
      getPosts()
      .then((posts) => {
        res.write(JSON.stringify(posts))
        res.end()
      })
    
      axios({
        method: 'get',
        baseURL: process.env.POSTS_SERVICE,
      })
      .then((posts) => {
        res.write(JSON.stringify(posts))
        res.end()
      })
    }

## Deployment nach AWS    
    
    # DOCKER MACHINE
    docker-machine create --driver amazonec2 --amazonec2-open-port 80 my-playground
    docker-machine env my-playground
    docker version
    docker images
    
    docker build -t my/prototype .
    docker run -d -p 80:3000 --restart always --name my-prototype my/prototype

## Lokale Entwicklungumgebung II

Alternative Resourcen mit Docker Compose:

- docker-compose.yml beschreibt unsere lokale Entwicklungsumgebung
- Nuxt-Task aus dem Dockerfile bauen
- weiteren Task (exemplarisch aus Image erstellt) anbinden
- noch keine Idee, was genau Beispiel sein soll

## Fragen

    Marcus Baer <marcus.baer@publicispixelpark.de>
